import os

from . import lab

INTERFACES_DIR = os.path.join('etc', 'network')
INTERFACES_FILENAME = "interfaces"

def create_etc_network(lab_name, host):
    os.makedirs(os.path.join(lab.get_lab_directory(lab_name), host, INTERFACES_DIR))

def create_interfaces_static(lab_name, interfaces_static):
    for host in interfaces_static:
        create_etc_network(lab_name, host)
        
        # lab_name/host/etc/network/interfaces
        interfaces_file = os.path.join(lab.get_lab_directory(lab_name), host, INTERFACES_DIR, INTERFACES_FILENAME)
        with open(interfaces_file, "w") as interfaces_writer:
            interfaces_writer.write(f"auto eth0\n"
                                    "iface eth0 inet static\n"
                                    "\taddress ip/subnet")

        # default route
        startup_file = os.path.join(lab.get_lab_directory(lab_name), f"{host}.startup")
        with open(startup_file, "a") as startup_writer:
            startup_writer.write("ip r add default via next_hop_ip\n")
            
def create_interfaces_dhcp(lab_name, interfaces_dhcp):
    for host in interfaces_dhcp:
        create_etc_network(lab_name, host)

        interfaces_file = os.path.join(lab.get_lab_directory(lab_name), host, INTERFACES_DIR, INTERFACES_FILENAME)
        with open(interfaces_file, "w") as interfaces_writer:
            interfaces_writer.write(f"auto eth0\n"
                                    "iface eth0 inet dhcp")

def configure_ip_static(lab_name, ip_static):
    for host in ip_static:
        startup_file = os.path.join(lab.get_lab_directory(lab_name), f"{host}.startup")
        with open(startup_file, "a") as startup_writer:
            startup_writer.write(f"ip addr add ip/subnet dev eth0\n"
                                  "ip r add default via next_hop_ip\n"
                                  "ip r add ip/subnet via next_hop_ip\n")