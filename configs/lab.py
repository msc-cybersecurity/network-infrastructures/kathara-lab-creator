import os
import shutil

NETWORKING_RESTART_CMD = "/etc/init.d/networking restart\n"

def get_lab_directory(lab_name):
    return os.path.join(os.curdir, lab_name)


def create_lab_directory(lab_name):
    os.makedirs(get_lab_directory(lab_name))


def create_lab_conf(lab_name, all_hosts):
    lab_conf_file = os.path.join(get_lab_directory(lab_name), "lab.conf")
    with open(lab_conf_file, "w") as lab_conf_writer:
        for host in all_hosts:
            lab_conf_writer.write(f"{host}[0]=A\n")

def create_startup(lab_name, all_hosts):
    for host in all_hosts:
        startup_file = os.path.join(get_lab_directory(lab_name), f"{host}.startup")
        with open(startup_file, "w") as startup:
            startup.write(NETWORKING_RESTART_CMD)

def destroy_lab(lab_dir):
    if os.path.exists(lab_dir):
        shutil.rmtree(lab_dir)