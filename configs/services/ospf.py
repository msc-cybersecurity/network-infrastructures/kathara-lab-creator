import os

from .. import lab

QUAGGA_DIR = os.path.join("etc", "quagga")

QUAGGA_DAEMONS_FILE = "daemons"
ZEBRA_FILE = "zebra.conf"
OSPFD_FILE = "ospfd.conf"

def configure_quagga_daemons(lab_name, host):
    # lab_name/host/etc/quagga/daemons
    quagga_daemons_file = os.path.join(lab.get_lab_directory(lab_name), host, QUAGGA_DIR, QUAGGA_DAEMONS_FILE)
    with open(quagga_daemons_file, "w") as quagga_daemons_writer:
        quagga_daemons_writer.write(f"zebra=yes\n"
                                    "bgpd=no\n"
                                    "ospfd=yes\n"
                                    "ospf6d=no\n"
                                    "ripd=no\n"
                                    "ripngd=no\n"
                                    "isisd=no\n"
                                    "ldpd=no\n")

    startup_file = os.path.join(lab.get_lab_directory(lab_name), f"{host}.startup")
    with open(startup_file, "a") as startup:
        startup.write("/etc/init.d/quagga restart\n")

def configure_zebra(lab_name, host):
    # lab_name/host/etc/quagga/zebra.conf
    zebra_conf_file = os.path.join(lab.get_lab_directory(lab_name), host, QUAGGA_DIR, ZEBRA_FILE)
    with open(zebra_conf_file, "w") as zebra_conf_writer:
        zebra_conf_writer.write(f"hostname {host}\n"
                                "password zebra\n"
                                "enable password zebra\n"
                                "\n"
                                "interface eth0\n"
                                "ip address address/netmask\n"
                                "link-detect")

def configure_ospfd(lab_name, host):
    # lab_name/host/etc/quagga/ospfd.conf
    ospfd_conf_file = os.path.join(lab.get_lab_directory(lab_name), host, QUAGGA_DIR, OSPFD_FILE)
    with open(ospfd_conf_file, "w") as ospfd_conf_writer:
        ospfd_conf_writer.write(f"hostname {host}\n"
                                "password zebra\n"
                                "\n"
                                "interface eth0\n"
                                "ospf hello-interval 2\n"
                                "\n"
                                "router ospf\n"
                                "network 10.10.2.0/24 area 0.0.0.0")

def create_ospf_config(lab_name, ospf_host):
    for host in ospf_host:
        # lab_name/host/etc/quagga
        os.makedirs(os.path.join(lab.get_lab_directory(lab_name), host, QUAGGA_DIR))
        configure_quagga_daemons(lab_name, host)
        configure_zebra(lab_name, host)
        configure_ospfd(lab_name, host)