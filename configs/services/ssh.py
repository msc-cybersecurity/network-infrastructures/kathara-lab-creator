import os

from .. import lab

def create_user(lab_name, host, user, password):
    startup_file = os.path.join(lab.get_lab_directory(lab_name), f"{host}.startup")
    with open(startup_file, "a") as startup:
        startup.write(f"/etc/init.d/ssh restart\n"
                       "useradd {user} -m\n"
                       "echo -e '{password}\\n{password}\\n | passwd {user}'\n")
