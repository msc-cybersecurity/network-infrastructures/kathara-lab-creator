import os

from .. import lab

DHCPD_DIR = os.path.join('etc', 'dhcp')
DHCPD_FILENAME = "dhcpd.conf"

def create_dhcpd_config(lab_name, dhcp_server):
    for host in dhcp_server:
        # lab_name/host/etc/dhcp
        os.makedirs(os.path.join(lab.get_lab_directory(lab_name), host, DHCPD_DIR))

        # lab_name/host/etc/dhcp/dhcpd.conf
        dhcpd_file = os.path.join(lab.get_lab_directory(lab_name), host, DHCPD_DIR, DHCPD_FILENAME)

        with open(dhcpd_file, "w") as dhcpd_writer:
            dhcpd_writer.write(f"default-lease-time 3600;\n"
                              "\n" 
                              "subnet 192.168.4.0 netmask 255.255.255.128 {\n"
                              "\trange 192.168.4.20 192.168.4.50;\n"
                              "\toption routers 192.168.4.1;\n"
                              "}")

        startup_file = os.path.join(lab.get_lab_directory(lab_name), f"{host}.startup")
        with open(startup_file, "a") as startup:
            startup.write("/etc/init.d/isc-dhcp-server start\n")