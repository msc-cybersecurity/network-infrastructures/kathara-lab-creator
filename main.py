#!/usr/bin/env python3

import os

from configs import lab, networking
from configs.services import dhcpd, ospf, ssh

LAB_NAME = 'lab01'

SSH_USER = "ssh_user"
SSH_PASSWORD = "ilovessh"

interfaces_static = [ 'r1', 'r2', 'r3', 'r4' ]
interfaces_dhcp = [ 'pc1', 'pc2' ]
ip_static = [ 's1', 's2' ]
dhcp_server = [ 'r1' ]
ospf_host = ['r5']

ssh_on_hosts = ['pc1', 'pc2']

all_hosts = interfaces_static + interfaces_dhcp + ip_static + ospf_host

def main():
    lab.create_lab_directory(LAB_NAME)
    lab.create_lab_conf(LAB_NAME, all_hosts)
    lab.create_startup(LAB_NAME, all_hosts)

    networking.create_interfaces_static(LAB_NAME, interfaces_static)
    networking.create_interfaces_dhcp(LAB_NAME, interfaces_dhcp)
    networking.configure_ip_static(LAB_NAME, ip_static)

    dhcpd.create_dhcpd_config(LAB_NAME, dhcp_server)

    ospf.create_ospf_config(LAB_NAME, ospf_host)

    for host in ssh_on_hosts:
        ssh.create_user(LAB_NAME, host, SSH_USER, SSH_PASSWORD)

if __name__ == '__main__':
    lab.destroy_lab(LAB_NAME)
    main()